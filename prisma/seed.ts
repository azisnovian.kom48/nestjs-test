/* eslint-disable prettier/prettier */
import { PrismaClient } from "@prisma/client";
import { faker } from "@faker-js/faker";
import * as bcrypt from 'bcrypt';

const prisma = new PrismaClient();

async function main() {

    const hashedPassword = await bcrypt.hash('password', 10);

    // Clear All
    await prisma.address.deleteMany();
    await prisma.user.deleteMany();

    // Seed 1
    await prisma.user.create({
        data: {
            id: faker.datatype.uuid(),
            username: 'glovory',
            password: hashedPassword,
            email: 'job@glovory.com',
            name: 'Glovory Job Portal',
            Address: {
                create: [{
                    id: faker.datatype.uuid(),
                    address: 'Victory Street 27',
                    city: 'Malang'
                },{
                    id: faker.datatype.uuid(),
                    address: 'Awesome Street 99',
                    city: 'Riyadh'
                }]
            }
        }
    });

    // Seed 2
    await prisma.user.create({
        data: {
            id: faker.datatype.uuid(),
            username: 'backend',
            password: hashedPassword,
            email: 'backend@glovory.com',
            name: 'Glovory Backend Team'
        }
    });
    
}

main()
.catch((e)=> {
    console.log(e);
    process.exit(1);
})
.finally(async ()=> {
    await prisma.$disconnect();
});
