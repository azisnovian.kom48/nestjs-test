import { Module } from '@nestjs/common';
import { AddressService } from './address.service';
import { AddressController } from './address.controller';
import { UserService } from 'src/user/user.service';

@Module({
  providers: [AddressService, UserService],
  controllers: [AddressController],
})
export class AddressModule {}
