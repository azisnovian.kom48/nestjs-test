/* eslint-disable prettier/prettier */
import { IsNotEmpty } from 'class-validator';

export class AddressDto {
  @IsNotEmpty()
  address: string;

  @IsNotEmpty()
  city: string;
}
