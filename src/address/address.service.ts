import { Injectable } from '@nestjs/common';
import { User } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { AddressDto } from './dto';

@Injectable()
export class AddressService {
  constructor(
    private prismaService: PrismaService,
  ) {}

  /**
   * Get user list
   * @param userId string
   * @returns User
   */
  async list(userId: string) {
    const address =
      await this.prismaService.address.findMany({
        where: {
          user_id: userId,
        },
      });
    return address;
  }

  /**
   * Create address of user
   * @param user User
   * @param dto AddressDtp
   * @returns User
   */
  async add(user: User, dto: AddressDto) {
    try {
      return await this.prismaService.user.update(
        {
          where: {
            id: user.id,
          },
          data: {
            Address: {
              create: {
                address: dto.address,
                city: dto.city,
              },
            },
          },
          select: {
            id: true,
            name: true,
            email: true,
            Address: true,
          },
        },
      );

      //   return await this.prismaService.user.findFirst(
      //     {
      //       where: {
      //         id: user.id,
      //       },
      //       select: {
      //         id: true,
      //         name: true,
      //         email: true,
      //         Address: true,
      //       },
      //     },
      //   );
    } catch (error) {
      throw error;
    }
  }
}
