import {
  Body,
  Controller,
  Get,
  Post,
  UseGuards,
} from '@nestjs/common';
import { JwtGuard } from 'src/auth/guard';
import { UserService } from 'src/user/user.service';
import { AddressService } from './address.service';
import { AddressDto } from './dto';

@UseGuards(JwtGuard)
@Controller('address')
export class AddressController {
  constructor(
    private addressService: AddressService,
    private userService: UserService,
  ) {}

  /**
   * Get list address of user
   */
  @Get('/:user_id')
  index(user_id: string) {
    return this.addressService.list(user_id);
  }

  @Post('/:user_id')
  async store(
    user_id: string,
    @Body() dto: AddressDto,
  ) {
    const user = await this.userService.detail(
      user_id,
    );
    return this.addressService.add(user, dto);
  }
}
